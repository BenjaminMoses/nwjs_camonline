/*
* To run, you need to run appium server via node and then run this test file, node .\appiumTest.js
* "Appium" in node command prompt. Then "node .\appiumTest.js" in vis studio command prompt
*/
const wdio = require('webdriverio');

// javascript
const opts = {
    port: 4723 ,
    desiredCapabilities: {
      platformName: "Android",
      platformVersion: "7.0",
      deviceName: "Galaxy A5 (2017)",
      browserName: "Chrome",
      automationName: "UiAutomator2"
    }
  };
  
  const client = wdio.remote(opts);
 
  // javascript
client
    .init()
    .url('https://cloud.ixara.com/')
    .waitForVisible(".header2" , 6000)
    .pause(500)
    .click('[name=\'fullname\']').keys('Benjamin Moses')
    .click('[name=\'email\']').keys('testben@xara.com')
    .click('[name=\'password\']').keys('10doughnutS')
    .click('.signin-button')
    .waitForVisible("#picker-view", 6000)
    .waitForVisible(".loader", 6000, true)
    .pause(3000)
    .waitForVisible('.intercom-notifications-dismiss-button', 2000)
    .pause(500)
    .click('*=Clear')
    //.click('*=Continue')
    .click('[data-filename="Add cloud drive"]')
    .pause(500)
    .click('[data-filename="Google Drive"]')
    .waitForVisible('#picker-view', 10000)
    .click('[data-filename="Introduction"]')
    .pause(550)
    .click('[title="Introduction"]')
    .waitForVisible('.bright-popup')
    .pause(550)
    .click('.blue-button')
    .element('.slideshow-slide-content-box').click("*=Skip tour")
client
    .click('[title="Settings"]')
    .click('[data-submenu="advanced"]')
    .click('*=Delete this account')
    .pause(500)
    .click('*=Delete')
    .pause(6000)
    .end();

/*
client
    .init()
    .url('https://en.wikipedia.org/')
    .click('#searchIcon')
    .pause(1000)
    .$('[name="search"]').setValue('Philosophy')
    .pause(2500)
    .setValue('[name="search"]', 'testcomplete')
    .pause(5500)
    .end();
*/