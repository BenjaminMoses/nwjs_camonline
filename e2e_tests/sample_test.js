module.exports = {
    "Sample e2e tests" : function(client){
        let signInShowing;
        client
            .url("https://cloud.ixara.com/")
            .waitForElementVisible(".header2", 6000)
            .assert.title('Create Stunning Documents | Xara Online Designer')
        client
            .element("css selector", ".header2 span", function(x){

                if(x.textContent == "Sign in"){
                    console.log('Sign in page is showing, switch to register');
                    client
                    .click('.header2 a')
                    .waitForElementVisible("[name='fullname']", 6000)
                }else {
                    console.log('Register page is showing.');
                }
            })
        client
            .getAttribute('.signin-button', "disabled", function(result){
                this.assert.equal(result.value, "true");
            })
            .setValue("[name='fullname']","Benjamin Moses")
            .setValue("[name='email']","ben@xara.com")
            .setValue("[name='password']","10doughnutS")
        client 
            .getAttribute('.signin-button', "disabled", function(result){
                this.assert.equal(result.value, null);
            })
            .click('.signin-button')
        client
            .waitForElementVisible("#picker-view", 6000)
            .waitForElementNotPresent(".loader", 6000)
            .pause(500) //allow for animation
            .getText(".filepicker-title", function(result){
                this.assert.equal(result.value, "Open a document")
            })
            .elements('css selector', 'article', function(results) {
                results.value.forEach(function(element) {
                    client.elementIdText(element.ELEMENT, function (result) {
                            // if word found.
                            if(result.value == "Introduction") {
                                // move the cursor on the same element and can perform click.
                                client.moveTo(element.ELEMENT, 0,0 , function () {
                                        client.elementIdClick(element.ELEMENT, function (clicked) { 
                                            console.log("You clicked on element a element which satisfy your if condition.");                         
                                        });
                                });
                            }
                    })
                });
            })
            .waitForElementNotPresent(".loader", 6000)
            .pause(500)
        client
            .elements('css selector', 'article', function(results) {
                results.value.forEach(function(element) {
                    client.elementIdText(element.ELEMENT, function (result) {
                            // if word found.
                            if(result.value == "Introduction") {
                                // move the cursor on the same element and can perform click.
                                client.moveTo(element.ELEMENT, 0,0 , function () {
                                        client.elementIdClick(element.ELEMENT, function (clicked) { 
                                            console.log("You clicked on element a element which satisfy your if condition.");                         
                                        });
                                });
                            }
                    })
                });
            })
            .waitForElementVisible('#addNewPage', 10000)
            .pause(5000)
        client.end(); 
    },
};